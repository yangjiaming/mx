<?php
namespace App\Models\Entity;

use Swoft\Db\Model;
use Swoft\Db\Bean\Annotation\Column;
use Swoft\Db\Bean\Annotation\Entity;
use Swoft\Db\Bean\Annotation\Id;
use Swoft\Db\Bean\Annotation\Required;
use Swoft\Db\Bean\Annotation\Table;
use Swoft\Db\Types;

/**
 * @Entity()
 * @Table(name="book")
 * @uses      Book
 */
class Book extends Model
{
    /**
     * @var int $id 
     * @Id()
     * @Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string $bookName 书名
     * @Column(name="book_name", type="string", length=50)
     */
    private $bookName;

    /**
     * @var int $ifTop 火热
     * @Column(name="if_top", type="integer", default=0)
     */
    private $ifTop;

    /**
     * @var string $type 类型
     * @Column(name="type", type="string", length=10)
     */
    private $type;

    /**
     * @var string $status 状态
     * @Column(name="status", type="string", length=10)
     */
    private $status;

    /**
     * @var string $introduction 简介
     * @Column(name="introduction", type="string", length=3000)
     */
    private $introduction;

    /**
     * @var float $numberWords 字数，单元:万
     * @Column(name="number_words", type="double")
     */
    private $numberWords;

    /**
     * @var string $coverUrl 封面地址
     * @Column(name="cover_url", type="string", length=50)
     */
    private $coverUrl;

    /**
     * @var string $author 作者
     * @Column(name="author", type="string", length=50)
     */
    private $author;

    /**
     * @var string $wId 
     * @Column(name="w_id", type="string", length=100)
     */
    private $wId;

    /**
     * @var string $lastChapter 
     * @Column(name="last_chapter", type="string", length=200)
     */
    private $lastChapter;

    /**
     * @var int $createTime 
     * @Column(name="create_time", type="integer")
     */
    private $createTime;

    /**
     * @var int $updateTime 
     * @Column(name="update_time", type="integer")
     */
    private $updateTime;

    /**
     * @var string $majorCate 
     * @Column(name="major_cate", type="string", length=100)
     */
    private $majorCate;

    /**
     * @var string $minorCate 
     * @Column(name="minorCate", type="string", length=100)
     */
    private $minorCate;

    /**
     * @param int $value
     * @return $this
     */
    public function setId(int $value)
    {
        $this->id = $value;

        return $this;
    }

    /**
     * 书名
     * @param string $value
     * @return $this
     */
    public function setBookName(string $value): self
    {
        $this->bookName = $value;

        return $this;
    }

    /**
     * 火热
     * @param int $value
     * @return $this
     */
    public function setIfTop(int $value): self
    {
        $this->ifTop = $value;

        return $this;
    }

    /**
     * 类型
     * @param string $value
     * @return $this
     */
    public function setType(string $value): self
    {
        $this->type = $value;

        return $this;
    }

    /**
     * 状态
     * @param string $value
     * @return $this
     */
    public function setStatus(string $value): self
    {
        $this->status = $value;

        return $this;
    }

    /**
     * 简介
     * @param string $value
     * @return $this
     */
    public function setIntroduction(string $value): self
    {
        $this->introduction = $value;

        return $this;
    }

    /**
     * 字数，单元:万
     * @param float $value
     * @return $this
     */
    public function setNumberWords(float $value): self
    {
        $this->numberWords = $value;

        return $this;
    }

    /**
     * 封面地址
     * @param string $value
     * @return $this
     */
    public function setCoverUrl(string $value): self
    {
        $this->coverUrl = $value;

        return $this;
    }

    /**
     * 作者
     * @param string $value
     * @return $this
     */
    public function setAuthor(string $value): self
    {
        $this->author = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setWId(string $value): self
    {
        $this->wId = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setLastChapter(string $value): self
    {
        $this->lastChapter = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCreateTime(int $value): self
    {
        $this->createTime = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setUpdateTime(int $value): self
    {
        $this->updateTime = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setMajorCate(string $value): self
    {
        $this->majorCate = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setMinorCate(string $value): self
    {
        $this->minorCate = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 书名
     * @return string
     */
    public function getBookName()
    {
        return $this->bookName;
    }

    /**
     * 火热
     * @return int
     */
    public function getIfTop()
    {
        return $this->ifTop;
    }

    /**
     * 类型
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 状态
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * 简介
     * @return string
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }

    /**
     * 字数，单元:万
     * @return float
     */
    public function getNumberWords()
    {
        return $this->numberWords;
    }

    /**
     * 封面地址
     * @return string
     */
    public function getCoverUrl()
    {
        return $this->coverUrl;
    }

    /**
     * 作者
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getWId()
    {
        return $this->wId;
    }

    /**
     * @return string
     */
    public function getLastChapter()
    {
        return $this->lastChapter;
    }

    /**
     * @return int
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @return int
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * @return string
     */
    public function getMajorCate()
    {
        return $this->majorCate;
    }

    /**
     * @return string
     */
    public function getMinorCate()
    {
        return $this->minorCate;
    }

}
