<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Models\Logic;

use App\Models\Common;
use App\Models\Entity\WebUser;
use Swoft\Bean\Annotation\Bean;
use Swoft\Rpc\Client\Bean\Annotation\Reference;

/**
 * 用户逻辑层
 * 同时可以被controller server task使用
 *
 * @Bean()
 * @uses      UserLogic
 * @version   2017年10月15日
 * @author    stelin <phpcrazy@126.com>
 * @copyright Copyright 2010-2016 swoft software
 * @license   PHP Version 7.x {@link http://www.php.net/license/3_0.txt}
 */
class UserLogic
{
    /**
     * @Reference("user")
     *
     * @var \App\Lib\DemoInterface
     */
    private $demoService;

    /**
     * @Reference(name="user", version="1.0.1")
     *
     * @var \App\Lib\DemoInterface
     */
    private $demoServiceV2;

    public function rpcCall()
    {
        return ['bean', $this->demoService->getUser('12'), $this->demoServiceV2->getUser('16')];
    }

    public function getUserInfo(array $uids)
    {
        $user = [
            'name' => 'boby',
            'desc' => 'this is boby'
        ];

        $data = [];
        foreach ($uids as $uid) {
            $user['uid'] = $uid;
            $data[] = $user;
        }

        return $data;
    }

    /***
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function register(array $params)
    {
        $user = WebUser::findOne(['username' => $params['username']], ['fields' => ['id']])->getResult();
        if($user) {
            throw new \Exception('该用户已存在', '-1');
        }
        $user = WebUser::findOne(['tel' => $params['tel']], ['fields' => ['id']])->getResult();

        if($user) {
            throw new \Exception('该手机号已存在', '-1');
        }

        if($params['cardno'] ) {
            if(!Common::isCreditNo($params['cardno'])) {
                return ['msg' => 0, 'code' => 0];
                //throw new \Exception('身份证输入有误', '-1');
            }
            //实名认证
            $host = "http://idcard.market.alicloudapi.com";
            $path = "/lianzhuo/idcard";
            $appcode = '4769ebbca42249d0b32b22cdb841bb9f';
            $query = "cardno={$params['cardno']}&name={$params['name']}";
            $url = $host . $path . "?" . $query;
            $headers = [];
            array_push($headers, "Authorization:APPCODE " . $appcode);
            $data = Common::httpGet($url, $headers);
            if($data) {
                $data = json_decode($data, 1);
                if($data['resp']['code'] != 0) {
                    return ['msg' => $data['resp']['desc'], 'code' => $data['resp']['code']];
                    //throw new \Exception($data['resp']['desc'], '-1');
                }
            }
        }

        $user   = new WebUser();
        $params['password'] = md5($params['password']);
        //$params['create_tiem'] = time();
        //$params['update_tiem'] = time();
        $result = $user->fill($params)->save()->getResult();
        if($result) {
            return ['code' => 200, 'msg' => '注册成功'];
        }
        return ['code' => -1, 'msg' => '注册失败'];
    }
}