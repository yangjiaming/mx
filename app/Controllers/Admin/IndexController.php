<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/22 0022
 * Time: 17:28
 */

namespace App\Controllers\Admin;

use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Http\Server\Bean\Annotation\RequestMapping;
use Swoft\View\Bean\Annotation\View;
use Swoft\Http\Message\Server\Request;
use Swoft\Http\Message\Bean\Annotation\Middleware;
use Swoft\Http\Server\Bean\Annotation\RequestMethod;
use Swoft\Bean\Annotation\ValidatorFrom;;

/**
 * Class IndexController
 * @Controller()
 */
class IndexController
{
    /**
     * @RequestMapping("/admin/index")
     * @View(template="admin/index")
     * @return array
     */
    public function index()
    {
        return ['name' => 'ss'];
    }

    /**
     * @RequestMapping("/admin/login")
     * @View(template="admin/login")
     * @return array
     */
    public function login()
    {
        return [];
    }
    /**
     * @RequestMapping(route="/admin/doSignIn", method={RequestMethod::POST,RequestMethod::PUT})
     * @param Request $request
     * @return array
     */
    public function doSignIn(Request $request)
    {
        $username = $request->post('username');
        $password = $request->post('password');
        return compact('username', 'password');
    }
}