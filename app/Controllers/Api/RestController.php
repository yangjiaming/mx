<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Controllers\Api;

use App\Models\Logic\UserLogic;
use App\Middlewares\CorsMiddleware;
use Swoft\Http\Message\Bean\Annotation\Middleware;
use Swoft\Http\Message\Server\Request;
use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Http\Server\Bean\Annotation\RequestMapping;
use Swoft\Http\Server\Bean\Annotation\RequestMethod;

/**
 * RESTful和参数验证测试demo.
 *
 * @Controller(prefix="/api/user")
 */
class RestController
{
    /**
     * 查询列表接口
     * 地址:/api/user/.
     *
     * @RequestMapping(route="/api/user", method={RequestMethod::GET})
     */
    public function list()
    {
        return ['list'];
    }

    /**
     * 创建一个用户
     * 地址:/api/user.
     *
     * @Middleware(CorsMiddleware::class)
     * @RequestMapping(route="/api/user", method={RequestMethod::POST,RequestMethod::PUT})
     *
     * @param Request $request
     *
     * @return array
     */
    public function create(Request $request)
    {
        /*$name = $request->input('username');

        $bodyParams = $request->getBodyParams();
        $bodyParams = empty($bodyParams) ? ['create', $name] : $bodyParams;*/
        $username = $request->input('username', '');
        $password = $request->input('password', '');
        $email = $request->input('email', '');
        $tel = $request->input('tel', '');
        $cardno = $request->input('cardno', '');
        $user = $request->input('user', '');

        $bodyParams = [
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'tel' => $tel,
            'cardno' => $cardno,
            'user' => $user
        ];
        if(empty($bodyParams)) {
            return ['code' => -1, 'msg' => '请求参数为空'];
        }
        $user = new UserLogic();
        $result = $user->register($bodyParams);
        return $result;
    }

    /**
     * 查询一个用户信息
     * 地址:/api/user/6.
     *
     * @RequestMapping(route="{uid}", method={RequestMethod::GET})
     *
     * @param int $uid
     *
     * @return array
     */
    public function getUser(int $uid)
    {
        return ['getUser', $uid];
    }

    /**
     * 查询用户的书籍信息
     * 地址:/api/user/6/book/8.
     *
     * @RequestMapping(route="{userId}/book/{bookId}", method={RequestMethod::GET})
     *
     * @param int    $userId
     * @param string $bookId
     *
     * @return array
     */
    public function getBookFromUser(int $userId, string $bookId)
    {
        return ['bookFromUser', $userId, $bookId];
    }

    /**
     * 删除一个用户信息
     * 地址:/api/user/6.
     *
     * @RequestMapping(route="{uid}", method={RequestMethod::DELETE})
     *
     * @param int $uid
     *
     * @return array
     */
    public function deleteUser(int $uid)
    {
        return ['delete', $uid];
    }

    /**
     * 更新一个用户信息
     * 地址:/api/user/6.
     *
     * @RequestMapping(route="{uid}", method={RequestMethod::PUT, RequestMethod::PATCH})
     *
     * @param int     $uid
     * @param Request $request
     *
     * @return array
     */
    public function updateUser(Request $request, int $uid)
    {
        $body           = $request->getBodyParams();
        $body['update'] = 'update';
        $body['uid']    = $uid;

        return $body;
    }
}
