<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/23 0023
 * Time: 15:26
 */

namespace App\Controllers;

use App\Models\Entity\BookType;
use Swoft\App;
use Swoft\Http\Message\Server\Request;
use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Http\Server\Bean\Annotation\RequestMapping;
use Swoft\Log\Log;
use Swoft\Task\Task;
use Swoft\View\Bean\Annotation\View;
use Swoft\Contract\Arrayable;
use Swoft\Http\Server\Exception\BadRequestException;
use Swoft\Http\Message\Server\Response;

/**
 * Class BookController
 * @Controller()
 */
class BookController
{
    /**
     * @RequestMapping(route="book/{uid}")
     */
    public function detail(int $uid,Request $request, Response $response)
    {

    }
    /**
     * @RequestMapping(route="/book/type")
     */
    public function getBookType()
    {
        $result = BookType::findAll([])->getResult();
        if(empty($result)) {
            $result = $this->getApiType();
            $data = [];
            foreach ($result as $key => $value) {
                if($key == 'male') {
                    $sex = 1;
                } elseif ($key == 'female') {
                    $sex = 0;
                } else {
                    continue;
                }
                foreach ($value as $v) {
                    $data[] = [
                        'name' => $v['name'],
                        'sex' => $sex,
                        'create_time' => time(),
                        'update_time' => time(),
                    ];
                }
            }
            BookType::batchInsert($data)->getResult();
        }
        return $result;
    }
    public function getApiType()
    {
        $result = file_get_contents('http://api.zhuishushenqi.com/cats/lv2/statistics');
        return json_decode($result, 1);
    }
    /**
     * @RequestMapping(route="/book/setTask")
     */
    public function setTask()
    {
        $result = Task::deliver('book', 'getBookList', ['玄幻', 'male', '0', '1'], Task::TYPE_ASYNC);
        return $result;
    }
}